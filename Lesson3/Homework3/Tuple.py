
def tuple_len_first(my_tuple):
    """1. Функция tuple_len_first . Принимает 1 аргумент: кортеж “ассорти” my_tuple (который создал пользователь).
     Возвращает кортеж состоящий из длины кортежа и первого элемента кортежа.
     Пример: (‘55’, ‘aa’, 66), результат (3, ‘55’)."""

    return (len(my_tuple), my_tuple[0])

def tuple_len_count_first(my_tuple, num):
    """2. Функция tuple_len_count_first .
     Принимает 2 аргумента: кортеж “ассорти” my_tuple (который создал пользователь) и число num.
      Возвращает кортеж состоящий из длины кортежа, количества чисел num в кортеже my tuple и первого элемента кортежа.
       Пример: my_tuple=(‘55’, ‘aa’, 66) num = 66, результат (3, 1, ‘55’)."""

    return (len(my_tuple), my_tuple.count(num), my_tuple[0])


def main():

    tpl = (111, 34, 5, "shdf g gh", "ewfsd", 1, 2, 5, 333)

    print(tuple_len_first(tpl))

    print(tuple_len_count_first(tpl, 5))

if __name__ == "__main__":
    main()