def list_count_num(my_list, num):
    """1. Функция list_count_num . Принимает 2 аргумента: список числами my_list (который создал
    пользователь) и число num.
     Возвращает количество num в списке my_list."""
    return list(my_list).count(num)


def list_count_word(my_list, word):
    """2. Функция list_count_word . Принимает 2 аргумента: список слов my_list
    (который создал пользователь) и строку word.
     Возвращает количество word в списке my_list."""

    return list(my_list).count(word)


def list_count_word_num(my_list):
    """3. Функция list_count_word_num . Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
     Возвращает количество чисел и количество слов(букв) в списке my_list."""

    amount_numbers = 0
    amount_words = 0
    amount_symbols = 0

    for i in my_list:

        if str(i).isdigit():
            amount_numbers += 1
        else:
            amount_words += len(str(i).split(" "))
            for _word in str(i).split(" "):
                amount_symbols += len(_word)

    return amount_numbers, amount_words, amount_symbols


def list_if_found(my_list, num, word):
    """4. Функция list_if_found . Принимает 3 аргумента: список “ассорти” my_list (который создал пользователь),
     число num и строку word. Возвращает “I found {word}”, если найдена только строка word; “I found {num}”,
      если найдено только число num; “I found {word} and {num}!!!”, если найдены оба;
      “Sorry, I can’t find anything...”, если не найдено ни слово ни число. (* тип num и word важен)."""


def list_magic_parts(my_list):
    """5. Функция list_magic_parts . Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
     Возвращает список, который состоит из
      [первые 2 элемента my_list] + [последний элемент my_list] + [количество элементов в списке my_list].
       Пример: входной список [1, 2, ‘aa’, ‘mm’], результат [1, 2, ‘mm’, 4]. (* Проверить существуют ли “первые 2”,
        если есть только 1, то берем его.
        Если список пустой - вернуть пустой список. Нумерация элементов начинается с 0 .)"""
    tmp_list = my_list[0:2]
    tmp_list.append(my_list[-1])
    tmp_list.append(len(my_list))
    return tmp_list


def list_magic_mul(my_list):
    """6. Функция list_magic_mul . Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
     Возвращает список, который состоит из
      [первого элемента my_list] + [три раза повторенных списков my_list] + [последнего элемента my_list].
       Пример: входной список [1, ‘aa’, 99], результат [1, 1, ‘aa’, 99, 1, ‘aa’, 99, 1, ‘aa’, 99, 99].
        (* Если индекс выходит за пределы списка, то последний. Если список пустой - вернуть пустой список.
         Нумерация элементов начинается с 0 .)"""
    tmp_list = (my_list * 3)
    tmp_list.insert(0, my_list[0])
    tmp_list.append(my_list[-1])
    return tmp_list



def list_magic_reversel(my_list):
    """7. Функция list_magic_reversel . Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
     Создает новый список new_list (копия my_list), порядок которого обратный my_list.
     Возвращает список, который состоит из
      [второго элемента new_list] + [предпоследнего элемента new_list] + [весь new_list].
       Пример: входной список [1, ‘aa’, 99], new_list [99, ‘aa’, 1], результат [1, 1, 99, ‘aa’, 1].
        (* Если индекс выходит за пределы списка, то последний. Если список пустой - вернуть пустой список.
         Нумерация элементов начинается с 0 .)"""
    new_list = my_list
    new_list.reverse()
    return new_list




def main():

    lst = [111, 34, 5, "shdf g gh", "ewfsd", 1, 2, 333]

    #print(list_count_num(lst, 1))

    #print(list_count_word_num(lst))

    #print(list_magic_parts(lst))

    #print(list_magic_mul(lst))

    #print(list_magic_reversel(lst))

    date_info = {'year': "2020", 'month': "01", 'day': "01"}
    track_info = {'artist': "Beethoven", 'title': 'Symphony No 5'}
    all_info = {**date_info}
    #, **track_info}
    print(*all_info)


    #for i, k in enumerate(lst):
     #   print(i, " ", k)

if __name__ == '__main__':
    main()