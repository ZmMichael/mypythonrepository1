def get_translation_by_word(ru_eng, ru_word):
    """ 3. Функция get_translation_by_word . Принимает 2 аргумента: ru-eng словарь содержащий ru_word:
    [eng_1, eng_2 ...] и слово для поиска в словаре (ru).
     Возвращает все варианты переводов, если такое слово есть в словаре, если нет,
      то ‘Can’t find Russian word: {word}’."""

    for _key, _volume in ru_eng.items():
        if _key == ru_word:
            return _volume

    return "Can’t find Russian word: " + ru_word



def get_translation_by_word1(ru_eng, ru_word):

    return dict(ru_eng).get(ru_word, "Can’t find Russian word: "  + ru_word)



def get_words_by_translation(ru_eng, eng_word):
    """4. Функция get_words_by_translation . ru-eng словарь содержащий ru_word: [eng_1, eng_2 ...] и слово
    для поиска в словаре (eng).
     Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng), если нет,
      то ‘Can’t find English word: {word}’."""

    for _key, _volume in ru_eng.items():
        if eng_word in _volume:
            return _volume

    return "Can’t find Russian word: " + ru_word

def main():

    dct = {str("имя")     : ["nane", "first name", "given name"],
           str("самолет") : ["aircraft", "plane", "airplane"],
           str("лес")     : ["forest", "wood"]}

    print(get_translation_by_word(dct, "колобок"))
    print(get_translation_by_word(dct, "лес"))

    print(get_translation_by_word1(dct, "колобок"))
    print(get_translation_by_word1(dct, "лес"))

    print(get_words_by_translation(dct, "plane"))


if __name__ == "__main__":
    main()