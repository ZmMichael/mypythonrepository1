def hello_with_default_vals(fname="Иван", lname="Иванов", age=18):
    """Определить функцию hello_with_default_vals.
     Принимает 3 аргумента: имя (по умолчанию “Иван”),фамилия (по умолчанию “Иванов”)
      и возраст (по умолчанию 18).
       Функция flower_with_default_valsвыводит строку в формате “Здравствуйте <имя> <фамилия>!
        <возраст> лет - самый лучший возраст!”.При этом в функции flower_with_default_vals есть
         проверка на то, что имя и фамилия - это строки (*Подсказка: type(x) == str или isinstance(s, str)),
          а также возраст - это число больше 0, но меньше 150. Вфункции main вызвать функцию flower_with_default_vals
           различными способами (перебратьварианты: имя, фамилия, возраст, имя фамилия, имя возраст, фамилия возраст,
            имя фамилиявозраст). (* Использовать именованные аргументы)"""

    if not (type(fname) == str and type(lname) == str and (type(age) == int or 0 < age < 150)):
        return None

    return f"Здравствуйте {fname} {lname}! {age} лет - самый лучший возраст!"


def main():

    print(hello_with_default_vals())
    print(hello_with_default_vals(18))
    print(hello_with_default_vals(age=88))



if __name__ == '__main__':
    main()



