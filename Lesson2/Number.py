def season(number_month):
    """* Функция принимает аргументы, которые перед этим ввел пользователь функцией input.
Функции. Числовые, логические типы и операции с ними. Условный оператор if.
1. Функция season . Принимает 1 аргумент — номер месяца (от 1 до 12), и возвращает время года,
которому этот месяц принадлежит (зима, весна, лето или осень).
2. Функция swap . Принимает 2 переменных с некоторыми значениями. Поменять местами значения этих
переменных и вывести на экран (например, “a = 3 | b = True”).
3. Функция check_numbers_by_5 . Принимает 3 числа. Если ровно два из них меньше 5, то вернуть True,
иначе вернуть False.
4. Функция check_equal . Принимает 3 числа. Вернуть True, если среди них есть одинаковые, иначе
False.
5. Функция count_positive . Принимает 3 числа. Вернуть количество положительных чисел среди них.
6. Функция sum_div_5 . Принимает 3 числа. Найти сумму тех чисел, которые делятся на 5. Если таких
чисел нет, то вывести error.
7. Функция if_7_yes_no . Принимает число. Если оно меньше 7, то вернуть “Yes”, если больше 10, то
вернуть “No”, если равно 9, то вернуть “Error”.
8. Функция check_plus_last_num . Принимает двузначное число. Вернуть результат вычисления
выражения: число + (последняя цифра числа).  """

    if number_month > 12 :
        return "Это уже не текущий год"

    if number_month < 3 or number_month == 12:
        return "Зима"

    if number_month < 6:
        return "Весна"

    if number_month < 9:
        return "Лето"


def swap(var1, var2):
    """* Функция принимает аргументы, которые перед этим ввел пользователь функцией input.
Функции. Числовые, логические типы и операции с ними. Условный оператор if.
2. Функция swap . Принимает 2 переменных с некоторыми значениями. Поменять местами значения этих
переменных и вывести на экран (например, “a = 3 | b = True”). """

    tmp_var = var1
    var1 = var2
    var2 = tmp_var

    print("Переменная 1 = ", var1)
    print("Переменная 2 = ", var2)

def check_numbers_by_5(number1, number2, number3):
    """3. Функция check_numbers_by_5 . Принимает 3 числа. Если ровно два из них меньше 5, то вернуть True, 
иначе вернуть False."""
    count = 0

    if number1 < 5:
        count += 1
    if number2 < 5:
        count += 1
    if number3 < 5:
        count += 1

    if count == 2:
        return True

    return False


def check_equal(number1, number2, number3):
    """4. Функция check_equal . Принимает 3 числа. Вернуть True, если среди них есть одинаковые, иначе False."""

    if number1 == number2:
        return True
    if number1 == number3:
        return True
    if number2 == number3:
        return True

    return False


def count_positive(number1, number2, number3):
    """5. Функция count_positive . Принимает 3 числа. Вернуть количество положительных чисел среди них."""
    count = 0

    if number1 > -1:
        count += 1
    if number2 > -1:
        count += 1
    if number3 > -1:
        count += 1

    return count


def sum_div_5(number1, number2, number3):
    """6. Функция sum_div_5 . Принимает 3 числа. Найти сумму тех чисел, которые делятся на 5. Если таких чисел нет, то вывести error."""
    sum_of_number = 0

    if number1 > 0 and number1 % 5 == 0:
        sum_of_number += number1
    if number2 > 0 and number2 % 5 == 0:
        sum_of_number += number2
    if number3 > 0 and number3 % 5 == 0:
        sum_of_number += number3

    if sum_of_number == 0:
        return "error"

    return sum_of_number


def if_7_yes_no(number):
    """ 7. Функция if_7_yes_no . Принимает число.
    Если оно меньше 7, то вернуть “Yes”,
     если больше 10, то вернуть “No”,
      если равно 9, то вернуть “Error”."""
    if number < 7:
        return "Yes"
    if number == 9:
        return "Error"
    if number > 10:
        return "No"

def check_plus_last_num(number1):
    """8. Функция check_plus_last_num . Принимает двузначное число.
     Вернуть результат вычисления выражения: число + (последняя цифра числа).  """


print(check_numbers_by_5(1,2,3))
print(check_numbers_by_5(1,8,3))
print(check_numbers_by_5(10,8,31))

print(check_equal(3,2,3))
print(check_equal(1,1,1))
print(check_equal(10,1,31))


print(count_positive(3,2,-1))

print(sum_div_5(5, 1, 3))
print(sum_div_5(5, 1, 25))
print(sum_div_5(1, 1, 1))

swap("defgwrgerg", True)

print(season(4))
print(season(98))
print(season(7))
print(season(12))