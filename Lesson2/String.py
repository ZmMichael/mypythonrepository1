def triple_sting_print_len(_string):
    """1. Функция triple_sting_print_len . Принимает строку.
     Вывести ее три раза через запятую и показать количество символов в ней."""

    for i in range(3):
        print(_string)

    print("Длинна строки : ", len(_string))


def print_3th_symbols(_string):
    """2. Функция print_3th_symbols . Принимает строку.
     Вывести третий, шестой, девятый и так далее символы."""

    for i in range(3, len(_string), 3):
        print("символ : ", _string[i])


def print_diff_count(string1, string2):
    """3. Функция print_diff_count . Принимает две строки.
     Вывести большую по длине строку столько раз,насколько символов отличаются строки."""

    if len(string1) > len(string2):
        for i in range(len(string1) - len(string2)):
            print(string1)
    else:
        for j in range(len(string2) - len(string1)):
            print(string2)




def modify_str_if_begin_abc(_string):
    """4. Функция modify_str_if_begin_abc . Принимает строку.
     Если она начинается на 'abc', то заменить их на 'www',
      иначе добавить в конец строки 'zzz'. Вернуть результат."""

    if _string[0] == 'a' and _string[1] == 'b' and _string[2] == 'c':
        return _string.replace("abc", "www", 1)
    else:
        return _string + "zzz"


def replace_word(_string):
    """5. Функция replace_word . Принимает строку.
     Возвращает строку, в которой вхождения 'word' заменены на 'letter'."""
    return _string.replace("word", "letter", len(_string))

def count_aba(_string):
    """ 6. Функция count_aba . Принимает строку. Возвращает количество вхождения 'aba' в строку."""
    count__ = 0
    for i in range(len(_string) - 3):
        if _string[i: i + 3: 1] == "aba":
            count__ += 1

    return count__

def remove_exclamation(_string):
    """7. Функция remove_exclamation . Удалите в строке все символы "!". Верните результат."""

    tmp_string = ''

    for i in list(_string):
        if not i == '!':
            tmp_string += str(i)

    return tmp_string


def count_words_by_spaces(_string):
    """8. Функция count_words_by_spaces . Принимает строку, состоящую из слов, разделенных пробелами.
    Возвращает количество слов в строке."""

    words_count = 0

    if not _string == " " or not _string == "":
        words_count += 1

    for i in list(_string):
        if i == ' ':
            words_count += 1

    return words_count


# triple_sting_print_len("rwegrew")

# print_3th_symbols("fwefgrewrgraebgare")

print_diff_count("dgsag", "fgdsfgdsg")
print_diff_count("dgregegergsag", "fgdsfgdsg")
print_diff_count("fgdsfgdsg1", "fgdsfgdsg")

#print(modify_str_if_begin_abc("dgasgfsdgdf"))
#print(modify_str_if_begin_abc("abcdgasgfsdgdf"))

#print(replace_word("rgerwgwordewgworda"))

#print("Количество подстрок 'aba' в строе ", count_aba("ferwgetjhrabatjhrejrejyrgabawe"))

#print(remove_exclamation("ffwrgre grewgerw rew!thrhreh"))

#print(" Количество слов в строке =", count_words_by_spaces("fdn пкта ерокт iwehf"))
