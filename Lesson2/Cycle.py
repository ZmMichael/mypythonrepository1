import random


def print_welcome(n):
    """
    1. Функция print_welcome . Принимает число n. Выведите на экран n раз фразу "You are welcome!".
    """
    index = n

    while index > 0:
        print("You are welcome!")
        index -= 1


def print_numbers_step1(n):
    """2. Функция print_numbers_step1 . Принимает число n. Выведите на экран числа от 1 до n (включительно) с шагом 1.
    """
    index = 1

    while index <= n:
        print(index)
        index += 1


def count_div_12(a, b):
    """ 3. Функция count_div_12 . Принимает целые числа a и b. Вернуть количество целых чисел от a до b
    включительно, которые делятся на 12. """

    if a >= b:
        return "Error"

    count_of_number = 0

    current_number = a

    while current_number < b:
        if not current_number % 12:
            count_of_number += 1
            # print(current_number)
        current_number += 1

    return count_of_number


def count_user_numbers():
    """ 4. Функция count_user_numbers . Пользователь вводит ненулевые числа до тех пор пока не введет
    ноль. Верните сумму этих чисел."""
    current_number = 1

    sum_of_number = 0

    while current_number:

        current_number = int(input("Введите текущее число : "))

        sum_of_number += current_number

    print("сумма чисел = ", sum_of_number)


def print_sum_3num(n):
    """5. Функция print_sum_3num . Принимает целое число n. Напечатайте трехзначные числа, сумма цифр
    которых равна n. Если нет ни одного такого, то напечатайте “Not found”."""

    not_found_number = True

    count_numer = 0

    if n <= (9 + 9 + 9):
        for i in range(1, 10):
            for j in range(0, 10):
                for k in range(0, 10):
                    if i + j + k == n:
                        #print(i * 100 + j * 10 + k)
                        not_found_number = False
                        count_numer += 1

    if not_found_number:
        print('Not found')

    print("Для числа ", n, "  -  ", count_numer)





def count_even_num(number):
    """6. Функция count_even_num . Принимает натуральное число. Верните количество четных цифр в этом числе."""
    number_of_even_digits = 0

    for i in list(str(number)):
        if not int(i) % 2:
            number_of_even_digits += 1

    return number_of_even_digits



def print_random_3():
    """ 7. Функция print_random_3 . Вывести 3 случайных числа от 0 до 100 без повторений."""

    count_rnd = 3

    rnd_number_mas = [0, 0, 0]

    while count_rnd:

        count_rnd -= 1

        rnd_number_mas[count_rnd] = random.randint(0, 100)




    print(rnd_number_mas)






def check_symb(curren_string, symbol):
    """8. Функция check_symb . Принимает строку и символ. Если символ не содержится в строке вывести
    сообщение “Not found!”, иначе “I found it!”."""

    for current_symbol in curren_string:

        if current_symbol == symbol:
            break
    else:
        print("Not found!")
        return
    print("I found it!")

for i in range(0, 28):

    print_sum_3num(i)

#print(count_even_num(49876353))
#print(count_even_num(7777777777777777755555555555555))
#print(count_even_num(333355523333333333))

# print_welcome(5)

# print_numbers_step1(7)

#print(count_div_12(36, 136))

#count_user_numbers()

#check_symb("efqwetgwerygew", 'e')

#print_random_3()