﻿def print_first_last_mid(_string):
    """"1. Функция print_first_last_mid . Принимает строку.
     Вывести первый, последний и средний (если он есть)) символы."""

    print("Первый символ : ", _string[0])
    if len(_string) % 2:
        print("Средний символ : ", _string[int(len(_string) / 2)])
    print("Последний символ : ", _string[-1])


def print_symbols_if(_string):
    """2. Функция print_symbols_if . Принимает строку.
     Вывести первые три символа и последний три символа, если длина строки больше 5.
      Иначе вывести первый символ столько раз, какова длина строки."""
    len_str = len(_string)

    if len_str > 5:
        print(_string[0: 3], " ", _string[-3:])
    else:
        while len_str:
            print(_string)
            len_str -= 1


def print_nth_symbols(_string, n):
    """3. Функция print_nth_symbols . Принимает строку и натуральное число n.
     Вывести символы с индексом n, n*2, n*3 и так далее."""
    count_iteration = int(len(_string) / n)

    if int(len(_string) % n):
        count_iteration += + 1

    for i in range(2, count_iteration):
        print(_string[n * i])


def print_after_sym(_string, sym):
    """4. Функция print_after_sym . Принимает строку произвольной длины и строку единичной длины sym.
    Определите общее количество символов после которых следует символ sym."""

    all_count_symbol = 0
    count_symbol = 0

    for i in range(len(_string)):
        if _string[i] == sym:
            all_count_symbol = i - count_symbol
            count_symbol += 1

    return all_count_symbol


def print_plus_minus_after0(_string):
    """5. Функция print_plus_minus_after0 . Принимает строку.
     Определите общее количество символов '+' и '-' в ней.
      А также сколько таких символов, после которых следует цифра ноль."""

    count_plus_minus = 0
    count_after_0 = 0
    current_count_after_0 = 0

    index = 0

    while len(_string) > index:

        if _string[index] == '+' or _string[index] == '-':
            count_plus_minus += 1

        if _string[index] == '0':
            count_after_0 += current_count_after_0  # - 1

        current_count_after_0 += 1

        index += 1

    print("Общее количество символов '+' и '-' : ", count_plus_minus)
    print("В данной строке ", count_after_0, " символов, после которых следует цифра ноль.")


def which_first_one_or_another(_string, one_sym, another_sym):
    """6. Функция which_first_one_or_another . Принимает строку произвольной длины и две строки единичной
    длины one_sym, another_sym.
     Определите и напечатайте, какой символ в ней встречается раньше: one_sym или another_sym.
      Если какого-то из символов нет, вывести сообщение вида “Couldn’t find symbol: ‘t’”."""

    print("Символ : ", one_sym, " ", str(_string).find(one_sym))
    print("Символ : ", another_sym, " ", str(_string).find(another_sym))

    position_one_sym = str(_string).find(one_sym)
    position_another_sym = str(_string).find(another_sym)

    if position_one_sym < 0:
        print("Couldn’t find symbol: ", one_sym)
    if position_another_sym < 0:
        print("Couldn’t find symbol: ", another_sym)

    if position_one_sym >= 0 and position_another_sym > 0:
        if position_one_sym < position_another_sym:
            print("Символ :", one_sym, " встречается раньше символа :", another_sym)
        else:
            print("Символ :", another_sym, " встречается раньше символа :", one_sym)

    # def modify_str_if_len_gt(_string, n):
    """7. Функция modify_str_if_len_gt . Принимает строку и целое положительное число n.
     Если ее длина больше n, то оставить в строке только первые 6 символов, иначе дополнить
      строку символами 'o' до длины 12."""


#    if len(_string) > int(n):
#        return _string[1: 6]
#    else:


def replace_even_odd_symbols(_string):
    """8. Функция replace_even_odd_symbols . Принимает строку.
     Заменить каждый четный символ или на 'a', если символ не равен 'a' или 'b',
      или на 'c' в противном случае. Вернуть результат."""

    string_len = len(_string)

    for i in range(string_len):
        if not i % 2:
            if _string[i] == 'a' or _string[i] == 'b':
                _string[i] = 'a'
            else:
                _string[i] = 'c'

    return _string


def count_numbers(_string):
    """9. Функция count_numbers . Принимает строку. Возвращает количество цифр в данной строке."""

    number_of_digits = 0

    for i in list(_string):
        if i >= '0' and i <= '9':
            number_of_digits += 1

    return number_of_digits


def check_abc(_string):
    """10. Функция check_abc . Принимает строку.
     Если строка содержит только символы 'a', 'b', 'c', то вернуть True, иначе False."""

    for i in list(_string):
        if not (i == 'a' or i == 'b' or i == 'c'):
            return False

    return True


def count_substr(origin, substr):
    """11. Функция count_substr . Принимает две строки origin и substr.
     Возвращает количество вхождений substr в строку origin."""

    return str(origin).find(substr)


def sub_numbers_in_str(_string):
    """12. Функция sub_numbers_in_str . Принимает строку.
     Возвращает сумму имеющихся в ней цифр."""

    sum_of_digits = 0

    for i in list(_string):
        if i >= '0' and i <= '9':
            sum_of_digits += int(i)

    return sum_of_digits


def symbols_a_b_caseup(_string):
    """13. Функция symbols_a_b_caseup . Принимает строку.
     Возвращает строку, где все символы 'a' и 'b' заменены на 'A' и 'B' соответственно."""

    return_string = ""

    for i in list(_string):

        if i == 'a':
            return_string += 'A'
        else:
            return_string += i

        if i == 'b':
            return_string += 'B'
        else:
            return_string += i

    return return_string

def check_substr(string1, string2):
    """14. Функция check_substr . Принимает две строки.
     Если меньшая по длине строка содержится в большей, то возвращает True, иначе False."""

    if len(string1) > len(string2):
        if str(string1).find(string2) < 0:
            return False
    else:
        if str(string2).find(string1) < 0:
            return False

    return True


def remove_question(_string):
    """15. Функция remove_question . Удалите в строке все символы "?". Верните результат."""

    return_string = ""

    for i in list(_string):
        if not (i == '?'):
            return_string += i

    return return_string



def count_words_by_symb(_string, symb):
    """17. Функция count_words_by_symb . Принимает две строки: строку, состоящую из слов,
     разделенных символом symb; сам символ symb. Возвращает количество слов в строке."""

    count_words = 0

    for i in list(_string):
        if i == symb:
            count_words += 1

    if not (_string[0] == symb):
        count_words += 1

    if _string[-1] == symb:
        count_words -= 1


    return count_words


def convert_to_roman(_string):
    """18. Функция convert_to_roman . Принимает строку.
     В строке записано десятичное число. Верните строку, где данное число записано римскими цифрами."""




def check_email(_string):
    """19. Функция check_email . Принимает email в строке.
     Определить, является ли он корректным (наличие символа @ и точки,
      наличие не менее двух символов после последней точки и т.д.).
       Возвращает True, если корректный, иначе False."""

    count_dog = 0
    count_dot = 0
    number_position_dot = 0

    for i in range(len(_string)):
        if _string[i] == '@':
            count_dog += 1

        if _string[i] == '.':
            count_dot += 1
            number_position_dot = i

    if not count_dog == 1:
        return False

    if len(_string) < number_position_dot + 3:
        return False

    return True


def split_triples_with_space(n):
    """20. Функция split_triples_with_space . Принимает натуральное число.
     Возвращает строку, в которой тройки цифр этого числа разделены пробелом,
      начиная с правого конца. Например, число 1234567 преобразуется в 1 234 567."""

    string_number = str(n)

    return_string = ""

    for i in range(len(string_number)):
        return_string = string_number[i]

    return return_string


def split_symb_with_space(_string):
    """21. Функция split_symb_with_space . Принимает строку.
     Вставить после каждого символа пробел.Вернуть получившуюся строку."""

    return_string = ""

    for i in list(_string):
        return_string += i
        return_string += " "

    return return_string


def check_alfabet_order(_string):
    """22. Функция check_alfabet_order . Принимает строку.
     Если символы в ней упорядочены по алфавиту, то вывести ‘YES’,
      иначе вывести первый символ, нарушающий алфавитный порядок."""

    current_symbol = _string[0]

    for i in range(1, len(_string)):
        if current_symbol <= _string[i]:
            current_symbol = _string[i]
        else:
            return False

    return True


def compare_strings(string1, string2):
    """23. Функция compare_strings . Принимает две строки.
     Сравнивает эти две строки. Возвращает True при равенстве, иначе False."""

    if not (len(string1) == len(string2)):
        return False

    index = 0

    while index < len(string1):
        if not (string1[index] == string2[index]):
            return False
        index += 1

    return True


# print_first_last_mid("gewrgwegwegwer")
# print_first_last_mid("gewrgwegwegwer1")
# print_first_last_mid("1234 5 6789")

##print_symbols_if("123456789")
##print_symbols_if("123")

# print_nth_symbols("0123456789", 3)

# print(print_after_sym("123y4567y891y23456789", 'y'))

# print_plus_minus_after0("012===+++3456+  -7890")

# which_first_one_or_another("ewrgewrgenrjdrawef", "a", "n")

# print(replace_even_odd_symbols("1234567890abcdef"))

# print("Количество цифр в строке : ", count_numbers("tehehea5shear53432r43het"))

#print(check_abc("regdshdshdr"))
#print(check_abc("aaabbbbccccaaaaabbbbbccc4"))



print(count_substr("1234567887534812387541321239", "123"))



#print(split_triples_with_space("13423"))

#print(count_substr("123456789012345678901234567890", "123"))

#print(sub_numbers_in_str("fewrq32tgre65dshdsf"))

#print(symbols_a_b_caseup("efwqgerhgthdbdf)"))

#print(symbols_a_b_caseup("efwqbgerahgthdbdf)"))

#print(check_substr("wefwqf", "ewfwqfwq"))

#print(check_substr("123", "95454123fwqfwer"))

#print(remove_question("gergre?gaer"))
#print(remove_question("few????????????gdserg"))

#print(count_words_by_symb("gerwgwhyjdsghrtjhtjektye", "e"))

#print(check_email("fwefwa@gewgew.fwef"))
#print(check_email("fwefwa@gew@gew.fc"))

#print(split_symb_with_space("gewrgewg"))
#print(split_symb_with_space("1234567890"))

# print(check_alfabet_order("rgfewrgewgadefgwe"))
# print(check_alfabet_order("123456789abcdef"))

# print(compare_strings("rgregeg",  "rgwegweer"))
# print(compare_strings("1234567890",  "1234567890"))
# print(compare_strings("1",  "1"))