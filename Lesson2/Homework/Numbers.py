"""
* Функция принимает аргументы, которые перед этим ввел пользователь функцией input.
Функции. Числовые, логические типы и операции с ними. Условный
оператор if.
"""

def arithmetic():
    """1. Функция arithmetic . Принимает 3 аргумента: первые 2 - числа, третий - операция,
     которая должна быть произведена над ними.
      Вернуть результат операции. Если третий аргумент +, сложить их; если—, то вычесть;
       * — умножить; / — разделить (первое на второе). В остальных случаях вернуть строку "Неизвестная операция"."""


""" 
2. Функция is_year_leap . Принимает 1 аргумент — год. Возвращает True, если год високосный, иначе 
False.
"""

def month(number):
    """3. Функция month . Принимает номер месяца.
     Возвращает название месяца, если месяца с таким номером не существует, то возвращает строку “Error”."""

    months = {1 : "Январь",
              2 : "Февраль",
              3 : "Март",
              4 : "Апрель",
              5 : "Май",
              6 : "Июнь",
              7 : "Июль",
              8 : "Август",
              9 : "Сентябр",
              10 : "Октябрь",
              11 : "Ноябрь",
              12 : "Декабрь"}



    if number > len(months):
        return "Error"
    else:
        return months[number]

import  calendar

def month2(number):
    """3. Функция month2 . Принимает номер месяца.
     Возвращает название месяца, если месяца с таким номером не существует, то возвращает строку “Error”."""

    if number < 1 or number > 12:
        return "Error"

    return calendar.month_name[number]


def print_cube_and_square(number):
    """4. Функция print_cube_and_square . Принимает число.
     Выведите на экран квадрат этого числа, куб этого числа."""

    print("Квадрат числа ", number, " = ", number * number)
    print("Куб числа ", number, "     = ", number * number * number)


def magic_arithmetic(num1, num2, num3):
    """5. Функция magic_arithmetic . Принимает 3 числа.
     Увеличьте первое число в два раза,
      второе число уменьшите на 3,
       третье число возведите в квадрат и
        затем найдите сумму новых трех чисел.
        Напечатайте результат."""

    print(num1 * 2 + ( num2 -3 ) + num3 * num3)


def celsius_to_fahrenheit(tc):
    """6. Функция celsius_to_fahrenheit . Принимает значение температуры в градусах Цельсия.
     Вернуть температуру в градусах Фаренгейта."""

    return int(tc * 1.8 + 32)

def if_3_do(number):
    """7. Функция if_3_do . Принимает число. Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
     Вернуть результат."""

    if number > 3:
        return number + 10

    return number - 10

""" 
8. Функция check_numbers_by_10 . Принимает 3 числа. Если все числа больше 10 и первые два числа 
делятся на 3, то вернуть True, иначе False.
"""
""" 
9. Функция check_numbers_by_magic . Принимает 4 числа, если первые два числа больше 5, третье 
число делится на 6, четвертое число не делится на 3, то вывести yes, иначе no.
"""
""" 
10. Функция max_number . Принимает 3 числа. Верните наибольшее число.
"""
""" 
11. Функция max_sum . Принимает 3 числа. Найдите те два из них, сумма которых наибольшая и верните 
ее как результат.
"""
""" 
12. Функция sum_div_n . Принимает 3 числа. Найти сумму тех чисел, которые делятся на n. Если таких 
чисел нет, то вывести error.
"""
""" 
13. Функция max_even_number . Принимает 4 числа. Найдите наибольшее четное число среди них и 
верните его. Если оно не существует, верните фразу "not found".
"""
""" 
14. Функция check_sum . Принимает 3 числа. Вернуть True, если можно взять какие-то два из них и в 
сумме получить третье, иначе False.
"""
""" 
15. Функция count_negative . Принимает 4 числа. Вернуть количество отрицательных чисел среди них.
"""
""" 
16. Функция check_num_palindrome . Принимает четырехзначное число. Если оно читается слева 
направо и справа налево одинаково, то вернуть True, иначе False. 
"""
"""
17. Функция check_diff_100 . Принимает 2 числа. Вернуть True, если они отличаются на 100, иначе False.
"""
""" 
18. Функция check_minus_last_num . Принимает трехзначное число. Вернуть результат вычисления 
выражения: число - (последняя цифра числа). 
"""


#print(month(8))
#print(month(15))

#print(month2(15))
#print(month2(1))

#print_cube_and_square(8)

#magic_arithmetic(1, 2, 3)

#print(celsius_to_fahrenheit(36.6))
#print(celsius_to_fahrenheit(119.445))

print(if_3_do(8))
print(if_3_do(2))