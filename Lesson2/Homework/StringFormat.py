def print_hello_name_age(first_name, last_name, age):
    """1. Функция print_hello_name_age . Принимает first_name, last_name, age.
     Возвращает строку в виде “Здравствуйте, Иванов Петр. Как хорошо, что вам всего 66!”. """

    return "Здравствуйте, {} {}.Как хорошо, что вам всего {}!".format(first_name, last_name, age)


def print_coordinates(x, y):
    """2. Функция print_coordinates . Принимает координаты x, y.
     Возвращает строку в виде “Цель: 56.879, 22.554”."""
    return "Цель: {}, {}".format(x, y)

def print_name_error(name, err_no):
    """"3. Функция print_name_error . Принимает name, err_no.
     Возвращает строку в виде “Hi, name. There is a 0xerr_no!”.
      (Например, при входных параметрах Ellen, 404 будет получена строка “Hi, Ellen. There is a 0x404!”)."""
    return "Hi, {}. There is a 0x{}!".format(name, err_no)

print(print_hello_name_age("rgewrgwer", "regewrgwe", 64))

print(print_coordinates(2, 5))

print(print_name_error("rgwegew", 49853))