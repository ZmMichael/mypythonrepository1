import random

def print_hi(n):
    """"1. Функция print_hi . Принимает число n.
     Выведите на экран n раз фразу "Hi, friend!"""

    for i in range(int(n)):
        print("Hi, friend!")


def print_numbers(start, end, step):
    """2. Функция print_numbers . Принимает 3 числа: start (начальное число),
     end (последнее число) и step(шаг).
      Выведите на экран числа от start (включительно) до end (включительно) с шагом step."""

    number = start

    while number <= end:
        print(number)
        number += step


def print_count_down_100():
    """3. Функция print_count_down_100 .
     Вывести на экран числа 100, 96, 92, ... до последнего положительного включительно."""

    number = 100

    while number >= 0:
        print(number)
        number -= 4


def sum_1_112_3():
    """4. Функция sum_1_112_3 . Вернуть сумму 1+4+7+11+...+112."""

    sum = 0

    for i in range(1, 112 + 3, 3):
        sum += i

    return sum



def print_mul_table(n):
    """5. Функция print_mul_table . Принимает число n. Выведите на экран таблицу умножения для числа n."""

    for i in range(10):
        print(i, " * ", int(n), " = ", i * int(n))


def count_div(a, b, d):
    """6. Функция count_div . Принимает целые числа a и b, d.
     Вернуть количество целых чисел от a до b включительно, которые делятся на d."""

    count_numbers = 0

    for i in range(a, b + 1):
        if not (i % d):
            count_numbers += 1

    return count_numbers


def count_user_even_numbers():
    """7. Функция count_user_even_numbers . Пользователь вводит ненулевые целые числа до тех пор,
     пока не введет ноль. Верните количество четных чисел, которые он ввел."""

    current_number = 1

    count_even_numbers = 0

    while not (int(current_number) == 0):

        current_number = input("введите число : ")

        # что делать с 0
        if not (int(current_number) % 2):
            count_even_numbers += 1

    return count_even_numbers


def print_sum_4num(n):
    """8. Функция print_sum_4num . Принимает целое число n.
     Напечатайте четырехзначные числа, сумма цифр которых равна n.
      Если нет ни одного такого, то напечатайте “Not found”."""

    not_found_number = True

    count_numer = 0

    if n <= (9 + 9 + 9 + 9):
        for i in range(1, 10):
            for j in range(0, 10):
                for k in range(0, 10):
                    for h in range(0, 10):
                        if i + j + k + h == n:
                            #print(i * 1000 + j * 100 + k * 10 + h)
                            not_found_number = False
                            count_numer += 1

    if not_found_number:
        print('Not found')

    print("Для числа ", n, "  -  ", count_numer)

def count_odd_num(n):
    """9. Функция count_odd_num . Принимает натуральное число.
     Верните количество нечетных цифр в этом числе."""

    string_number = str(n)

    count_of_odd_digits = 0

    for i in list(string_number):
        if int(i) % 2:
            count_of_odd_digits += 1

    return count_of_odd_digits


def count_num_between(n, min, max):
    """10. Функция count_num_between . Принимает числа n, min и max.
     Верните количество нечетных цифр в этом числе, которые больше min, но меньше max."""

    string_number = str(n)

    count_of_odd_digits = 0

    for i in list(string_number):
        if int(i) % 2 and int(i) > min and int(i) < max:
            count_of_odd_digits += 1

    return count_of_odd_digits


def print_random_2_between(min, max):
    """11. Функция print_random_2_between . Принимает числа min и max.
     Вывести 3 случайных числа от min до max (включительно) без повторений."""

    if min >= max:
        return

    for i in range(3):
        rnd = random.randint(min, max)
        print(rnd)


#print_hi(8)

#print_numbers(3, 100, 12)

#print_count_down_100()

#print(sum_1_112_3())

#print_mul_table(4)

#print(count_div(3, 100, 111))

#print(count_user_even_numbers())

#print(print_sum_4num(87))

#print(count_odd_num(654335))

#print(count_odd_num(11111111111111111))

#print(count_num_between(4657843154, 3, 9))

print_random_2_between(56, 57)