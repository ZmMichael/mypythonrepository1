def gen_dict_double(n):
    """1. Функция gen_dict_double . Принимает число n.
     Возвращает словарь длиной n, в котором ключ – это значение от 0 до n, а значение - удвоенное значение ключа.
     Пример: n=3, результат {0: 0, 1: 2, 2: 4}."""

    return {x: x * 2 for x in range(n)}

def gen_dict_double_start_stop(start, stop):
    """2. Функция gen_dict_double_start_stop . Принимает числа start, stop.
     Возвращает словарь, в котором ключ - это значение от start до stop (не включая),
      а значение - удвоенных значение ключа. Пример: start=3, stop=6, результат {3: 6, 4: 8, 5: 10}."""

    return {x: x * 2 for x in range(start, stop)}

def gen_dict_pow_start_stop(start, stop):
    """3. Функция gen_dict_pow_start_stop . Принимает числа start, stop.
     Возвращает словарь, в котором ключ - это значение от start до stop (не включая),
      а значение - это квадрат ключа . Пример: start=3, stop=6, результат {3: 9, 4: 16, 5: 25}."""

    return {x: x * x for x in range(start, stop)}

def gen_dict_pow_even(n):
    """4. Функция gen_dict_pow_even . Принимает число n.
     Возвращает словарь длиной n, в котором ключ - это четное число в диапазоне от 0 до n,
      а значение - квадрат ключа. Пример: n = 8, четные числа [0, 2, 4, 6], результат {0: 0, 2: 4, 4: 16, 6: 36}."""

    return {x: x * x for x in range(n) if not x % 2}

def gen_dict_pow_odd(n):
    """5. Функция gen_dict_pow_odd . Принимает число n.
     Возвращает словарь длиной n, в котором ключ - это нечетное число в диапазоне от 0 до n,
      а значение - квадрат ключа. Пример: n = 7, четные числа [1, 3, 5], результат {1: 1, 3: 9, 5: 25}."""

    return {x: x * x for x in range(n) if x % 2}




def main():

    print(gen_dict_double(5))

    print(gen_dict_double_start_stop(8, 20))

    print(gen_dict_pow_start_stop(6, 9))

    print(gen_dict_pow_even(9))

    print(gen_dict_pow_odd(12))

if __name__ == '__main__':
    main()