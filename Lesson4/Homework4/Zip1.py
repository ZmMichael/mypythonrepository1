from itertools import zip_longest

def zip_names(my_list, my_set):
    """1. Функция zip_names . Принимает 2 аргумента: список с именами и множество с фамилиями.
    Возвращает список с парами значений из каждого аргумента.
     (* Провести эксперименты с различными длинами входных данных.
      Если они одинаковые? А если разные? А если один из них пустой?)"""

    return list(zip(my_list, my_set))


def zip_colour_shape(my_list, my_tuple):
    """2. Функция zip_colour_shape . Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с
    формами (квадрат, круг).
     Возвращает список с парами значений из каждого аргумента. (* Провести эксперименты с различными
      длинами входных данных. Если они одинаковые? А если разные? А если один из них пустой?)"""

    return list(zip(my_list, my_tuple))


def zip_car_year(car_list, year_list):
    """3. Функция zip_car_year . Принимает 2 аргумента: список с машинами и список с годами производства.
    Возвращает список с парами значений из каждого аргумента, если один список больше другого, то заполнить
     недостающие элементы строкой “???”. (* Подсказка: zip_longest. Провести эксперименты с различными
      длинами входных данных. Если они одинаковые? А если разные? А если один из них пустой?)"""

    return list(zip_longest(car_list, year_list, fillvalue = "???"))


def main():

    lst = ["Name1", "Name2", "Name3", "Name4", "Name5", "Name6", "Name7"]

    st = {"Surname1", "Surname2", "Surname3", "Surname4", "Surname5"}

    print(zip_names(lst, st))

    color_list = ["зеленый", "красный"]
    figure_tuple = ("квадрат", "круг", "прямоугольник")

    print(zip_colour_shape(color_list, figure_tuple))

    color_list.append("синий")
    color_list.append("черный")
    print(zip_colour_shape(color_list, figure_tuple))

    print(zip_colour_shape(color_list, figure_tuple))

    car_list = ["Car1","Car2","Car3","Car4","Car5","Car6","Car7","Car8"]

    year_list = [1988, 1995, 1999, 2001, 2003]

    print(zip_car_year(car_list, year_list))

if __name__ == '__main__':
    main()