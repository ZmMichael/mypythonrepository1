def gen_list_double(n):
    """"1. Функция gen_list_double . Принимает число n.
     Возвращает список длиной n, состоящий из удвоенных значений от 0 до n. Пример: n=3, результат [0, 2, 4]."""

    return [x * 2 for x in range(n)]

def gen_list_double_start_stop(start, stop):
    """2. Функция gen_list_double_start_stop . Принимает числа start, stop.
     Возвращает список состоящий из удвоенных значений от start до stop (не включая).
      Пример: start=3, stop=6, результат [6, 8, 10]."""

    return [x * 2 for x in range(start, stop)]

def gen_list_pow_start_stop(start, stop):
    """3. Функция gen_list_pow_start_stop . Принимает числа start, stop.
     Возвращает список состоящий из квадратов значений от start до stop (не включая).
      Пример: start=3, stop=6, результат [9, 16, 25]."""

    return [x * x for x in range(start, stop)]


def gen_list_pow_even(n):
    """4. Функция gen_list_pow_even . Принимает число n.
     Возвращает список длиной n, состоящий из квадратов четных чисел в диапазоне от 0 до n.
      Пример: n = 8, четные числа [0, 2, 4, 6], результат [0, 4, 16, 36]."""

    return [x * x for x in range(n) if not x % 2]

def gen_list_pow_odd(n):
    """5. Функция gen_list_pow_odd . Принимает число n.
     Возвращает список длиной n, состоящий из квадратов нечетных чисел в диапазоне от 0 до n.
      Пример: n = 7, четные числа [1, 3, 5], результат [1, 9, 25]."""

    return [x * x for x in range(n) if x % 2]



def main():

    print(gen_list_double(5))

    print(gen_list_double(12))

    print(gen_list_double_start_stop(8, 17))

    print(gen_list_pow_start_stop(9, 20))

    print(gen_list_pow_even(18))

    print(gen_list_pow_odd(60))

if __name__ == '__main__':
    main()