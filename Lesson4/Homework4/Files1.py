
def save_list_to_file_classic(file_name, my_list):
    """1. Функция save_list_to_file_classic . Принимает 2 аргумента: строка (название файла или полный путь к файлу),
     список (для сохранения).
      Сохраняет список в файл. Проверить, что записалось в файл."""

    f = open(file_name, 'w')

    for i in my_list:
        f.write(str(i))
    f.close()

    f = open(file_name, 'r')

    for i in f:
        print(i, " из ", my_list)

    f.close()


def save_list_to_file_pickle(file_name, my_list):
    """2. Функция save_list_to_file_pickle . Принимает 2 аргумента: строка (название файла или полный путь к файлу),
     список (для сохранения). Сохраняет список в файл.
      Загрузить список и проверить его на корректность."""

    f = open(file_name, 'w')

    for i in my_list:
        f.write(str(i) + "\n")
    f.close()

    f = open(file_name, 'r')

    tmp = f.read().split("\n")

    print(my_list)
    print(tmp)

    f.close()


def save_dict_to_file_classic(file_name, my_dict):
    """3. Функция save_dict_to_file_classic . Принимает 2 аргумента: строка (название файла или полный путь к файлу),
     словарь (для сохранения). Сохраняет список в файл.
      Проверить, что записалось в файл."""



"""
4. Функция save_dict_to_file_pickle . Принимает 2 аргумента: строка (название файла или полный путь к 
файлу), словарь (для сохранения). Сохраняет список в файл. Загрузить словарь и проверить его на 
корректность. 
"""
"""
5. Функция read_str_from_file . Принимает 1 аргумент: строка (название файла или полный путь к 
файлу). Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и 
наполнен каким-то текстом.) """

def append_default(element, lst=[]):
    lst.append(element)
    return lst


def main():
    lst = [111, 34, 5, "shdf g gh", "ewfsd", 1, 2, 333]

    #save_list_to_file_classic("fil_list1.txt", lst)

    #save_list_to_file_pickle("fil_list2.txt", lst)

    print(append_default("1"))

    print(append_default("2"))


if __name__ == '__main__':
    main()