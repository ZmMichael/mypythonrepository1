from xxsubtype import spamdict


def convert_list_to_tuple(my_list, findData):
    """1. Функция convert_list_to_tuple . Принимает 2 аргумента: список (с повторяющимися элементами) и
     значение для поиска.
      Возвращает кортеж (из входного списка) и найдено ли искомое значение(True |False)."""

    return (my_list, True if list(my_list).count(findData) > 0 else False)


def convert_list_to_set(my_list):
    """2. Функция convert_list_to_set . Принимает 1 аргумент: список (с повторяющимися элементами).
    Возвращает множество (из входного списка) и количество элементов в множестве."""

    my_set = set(my_list)

    my_set.add(len(my_set))

    return my_set


def convert_list_to_dict(my_list, findData):
    """3. Функция convert_list_to_dict . Принимает 2 аргумента: список (с повторяющимися элементами) и
    значение для поиска.
     Возвращает словарь (из входного списка), в котором ключ=значение из списка, а
    значение=индекс этого элемента в списке, найдено ли искомое значение(True | False) в ключах
    словаря и найдено ли искомое значение(True | False) в значениях словаря, количество элементов в словаре."""

    my_dict = dict()

    for i in range(len(my_list)):
        if my_dict.get(my_list[i]):
            my_dict[my_list[i]] = [my_dict.get(my_list[i]), i]
        else:
            my_dict[my_list[i]] = i

    return my_dict, findData in my_dict.keys(), findData in my_dict.values(), len(my_dict)


def convert_list_to_str(my_list, slip_str):
    """4. Функция convert_list_to_str . Принимает 2 аргумента: список (с повторяющимися элементами) и
    разделитель (строка).
     Возвращает строку полученную разделением элементов списка разделителем, а также количество разделителей
      в получившейся строке в квадрате."""



def convert_tuple_to_list(my_tuple, findData):
    """5. Функция convert_tuple_to_list . Принимает 2 аргумента: кортеж (с повторяющимися элементами) и
     значение для поиска.
      Возвращает список (из входного кортежа) и найдено ли искомое значение (True| False)."""

    my_list = list(my_tuple)
    my_list.append(my_tuple[my_tuple.index(findData)])

    return my_list


def convert_tuple_to_set(my_tuple):
    """6. Функция convert_tuple_to_set . Принимает 1 аргумент: кортеж (с повторяющимися элементами).
    Возвращает множество (из входного кортежа) и количество элементов в множестве."""

    my_set = set(my_tuple)

    return my_set, len(set(my_tuple))

def convert_tuple_to_dict(my_tuple, findData):
    """7. Функция convert_tuple_to_dict . Принимает 2 аргумента: кортеж (с повторяющимися элементами) и
    значение для поиска.
     Возвращает словарь (из входного кортежа), в котором ключ=значение из кортежа,
      а значение=индекс этого элемента в кортеже, а также найдено ли искомое значение(True | False)
       в ключах словаря и найдено ли искомое значение(True | False) в значениях словаря."""

    my_dict = dict()

    for i in range(len(my_tuple)):
        if my_dict.get(my_tuple[i]):
            my_dict[my_tuple[i]] = [my_dict.get(my_tuple[i]), i]
        else:
            my_dict[my_tuple[i]] = i

    return my_dict, findData in my_dict.keys(), findData in my_dict.values(), len(my_dict)


def convert_tuple_to_str(my_tuple, split_string):
    """8. Функция convert_tuple_to_str . Принимает 2 аргумента: кортеж (с повторяющимися элементами) и
    разделитель (строка).
     Возвращает строку полученную разделением элементов кортежа
     разделителем, а также количество разделителей в получившейся строке в степени 3."""


def convert_set_to_list(my_set, find_data):
    """9. Функция convert_set_to_list . Принимает 2 аргумента: множество и значение для поиска.
     Возвращает список (из входного множества), а также True, если искомое значение НЕ найдено в списке, иначе False.
     """
    return list(my_set) + [True if list(my_set).count(find_data) > 0 else False]

def convert_set_to_tuple(my_set, find_data):
    """10. Функция convert_set_to_tuple . Принимает 2 аргумента: множество и значение для поиска.
    Возвращает список (из входного множества) повторенный 2 раза, а также True, если искомое значение
     найдено в списке, иначе False."""

    return list(my_set) * 2 + [True if list(my_set).count(find_data) > 0 else False]


def convert_set_to_dict(my_set, find_data):
    """11. Функция convert_set_to_dict . Принимает 2 аргумента: множество и значение для поиска.
     Возвращает словарь (из входного множества), в котором ключ=значение из множества, а значение=индекс этого
     элемента в множестве в квадрате, найдено ли искомое значение(True | False) в ключах словаря и
     найдено ли искомое значение(True | False) в значениях словаря, количество элементов в словаре."""

    my_list = list(my_set)

    my_dect = {}

    for i in range(len(my_list)):
        my_dect[my_list[i]] = i * i

    return my_dect, True if find_data in my_dect.keys() else False, True if find_data in my_dect.values() else False


""" 
12. Функция convert_set_to_str . Принимает 2 аргумента: множество и разделитель (строка). Возвращает 
строку полученную разделением элементов множества разделителем, а также количество 
разделителей в получившейся строке в степени 5. 
"""
""" 
13. Функция convert_dict_to_list . Принимает 1 аргумент: словарь (в котором значения повторяются). 
Возвращает список ключей, список значений, количество элементов в списке ключей, количество 
элементов в списке значений. 
"""
""" 
14. Функция convert_dict_to_tuple . Принимает 1 аргумент: словарь (в котором значения повторяются). 
Возвращает кортеж ключей, множество значений, а также True, если хотя бы один ключ равен одному 
из значений. 
"""
""" 
15. Функция convert_dict_to_set . Принимает 1 аргумент: словарь (в котором значения повторяются). 
Возвращает множество ключей, множество значений, количество элементов в множестве ключей, 
количество элементов в множестве значений. 
"""
""" 
16. Функция convert_dict_to_str . Принимает 1 аргумент: словарь (в котором значения повторяются). 
Возвращает строку вида “key1=val1 | key2 = val2 | key3 = val3”. (* Подсказка: помним про конкатенацию 
строк или zip).
"""

def main():

    lst = [111, 34, 5, "shdf g gh", "ewfsd", 1, 2, 333]

    tpl = (111, 34, 5, "shdf g gh", "ewfsd", 1, 2, 333, 34)

    st = {111, 34, 5, "shdf g gh", "ewfsd", 1, 2, 333}

    #print(lst)

    #print(convert_list_to_tuple(lst, "www"))

    #print(convert_list_to_set(lst))

    #print(convert_list_to_dict(lst, 34))

    #print(convert_tuple_to_list(tpl, 5))

    #print(convert_tuple_to_set(tpl))

    #print(convert_tuple_to_dict(tpl, 34))

    #print(convert_tuple_to_str(tpl, "8"))

    #print(convert_set_to_list(st, 2))

    #print(convert_set_to_tuple(st, 6))

    print(convert_set_to_dict(st, 8))



if __name__ == '__main__':
    main()
